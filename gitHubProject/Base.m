//
//  Base.m
//  gitHubProject
//
//  Created by Macbook Pro on 09/10/16.
//  Copyright © 2016 JoaoGabriel. All rights reserved.
//

#import "Base.h"

@implementation Base

+(NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{@"repository":@"repo"
             };
}

+ (NSValueTransformer *)baseJSONTransformer
{
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:[PullRequestRepositoryModel class]];
}

@end
