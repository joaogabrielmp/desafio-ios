//
//  JavaRepositoryModel.h
//  gitHubProject
//
//  Created by Banco Santander Brasil on 07/10/16.
//  Copyright © 2016 JoaoGabriel. All rights reserved.
//

#import <Mantle/Mantle.h>
#import "User.h"
@interface JavaRepositoryModel : MTLModel<MTLJSONSerializing>
@property (nonatomic,strong) NSString *repositoryDescription;
@property (nonatomic,strong) NSString *name;
@property (nonatomic,strong) User *user;
@property (nonatomic) float stars;
@property (nonatomic) float forks;
@end
