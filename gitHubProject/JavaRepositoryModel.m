//
//  JavaRepositoryModel.m
//  gitHubProject
//
//  Created by Banco Santander Brasil on 07/10/16.
//  Copyright © 2016 JoaoGabriel. All rights reserved.
//

#import "JavaRepositoryModel.h"

@implementation JavaRepositoryModel
+(NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{@"repositoryDescription":@"description",
             @"name":@"name",
             @"user":@"owner",
             @"stars":@"stargazers_count",
             @"forks":@"forks_count"};
}

+ (NSValueTransformer *)userJSONTransformer
{
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:[User class]];
}
@end
