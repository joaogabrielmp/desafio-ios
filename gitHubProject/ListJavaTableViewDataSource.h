//
//  ListJavaTableViewDataSource.h
//  gitHubProject
//
//  Created by Macbook Pro on 08/10/16.
//  Copyright © 2016 JoaoGabriel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "JavaRepositoryModel.h"
#import "ListJavaTableViewDataSourceDelegate.h"
@interface ListJavaTableViewDataSource : NSObject<UITableViewDataSource,UITableViewDelegate>

#pragma mark - properties

@property(nonatomic,weak)id<ListJavaTableViewDataSourceDelegate> delegate;
@property(nonatomic,strong)NSMutableArray<JavaRepositoryModel *> *listJavaRepository;

#pragma mark - constructors
-(instancetype)initWithTableView:(UITableView *)tableView;

@end
