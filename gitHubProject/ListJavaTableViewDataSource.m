//
//  ListJavaTableViewDataSource.m
//  gitHubProject
//
//  Created by Macbook Pro on 08/10/16.
//  Copyright © 2016 JoaoGabriel. All rights reserved.
//

#import "ListJavaTableViewDataSource.h"
#import "RepositoryCell.h"
@interface ListJavaTableViewDataSource ()
@property(nonatomic,strong)UITableView *tableView;
@end
@implementation ListJavaTableViewDataSource
-(instancetype)initWithTableView:(UITableView *)tableView {
    self = [super init];
    if (self) {
        self.tableView = tableView;
        [self.tableView setDelegate:self];
        [self.tableView setDataSource:self];
    }
    return self;
}

#pragma mark - setters


#pragma mark - table view data source
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.listJavaRepository.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 179.0f;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"RepositoryCell";
    RepositoryCell *repositoryCell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    JavaRepositoryModel *javaRepository = self.listJavaRepository[indexPath.row];
       if (repositoryCell == nil) {
        repositoryCell = [[RepositoryCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    [repositoryCell setJavaRepository:javaRepository];
    [repositoryCell setRepositoryInfo];
    return repositoryCell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.delegate didSelectJavaRepository:self.listJavaRepository[indexPath.row]];
    
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self.delegate didScroll:scrollView];
}
@end
