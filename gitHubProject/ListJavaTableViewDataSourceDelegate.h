//
//  ListJavaTableViewDataSourceDelegate.h
//  gitHubProject
//
//  Created by Macbook Pro on 08/10/16.
//  Copyright © 2016 JoaoGabriel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JavaRepositoryModel.h"
@protocol ListJavaTableViewDataSourceDelegate <NSObject>
-(void)didSelectJavaRepository:(JavaRepositoryModel *)javaRepository;
-(void)didScroll:(UIScrollView *)scrollView;
@end
