//
//  ListPullRequestRepository.h
//  gitHubProject
//
//  Created by Banco Santander Brasil on 06/10/16.
//  Copyright © 2016 JoaoGabriel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ListPullRequestRepositoryProtocol.h"
@interface ListPullRequestRepository : NSObject<ListPullRequestRepositoryProtocol>

@end
