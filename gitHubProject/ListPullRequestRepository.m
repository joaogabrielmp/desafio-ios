//
//  ListPullRequestRepository.m
//  gitHubProject
//
//  Created by Banco Santander Brasil on 06/10/16.
//  Copyright © 2016 JoaoGabriel. All rights reserved.
//

#import "ListPullRequestRepository.h"
#import <AFNetworking.h>

@implementation ListPullRequestRepository
+(void)getPullRequestsJavaRepository:(JavaRepositoryModel *)javaRepository success:(void (^)(NSArray<PullRequestModel *> *))_success failure:(void (^)(NSString *))_failure {
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager GET:[NSString stringWithFormat:@"https://api.github.com/repos/%@/%@/pulls",javaRepository.user.name,javaRepository.name] parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSArray<PullRequestModel*> *repositories = [MTLJSONAdapter modelsOfClass:[PullRequestModel class] fromJSONArray:responseObject error:nil];

        _success(repositories);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        _failure([error description]);
    }];
}
@end
