//
//  ListPullRequestRepositoryProtocol.h
//  gitHubProject
//
//  Created by Banco Santander Brasil on 06/10/16.
//  Copyright © 2016 JoaoGabriel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JavaRepositoryModel.h"
#import "PullRequestModel.h"
@protocol ListPullRequestRepositoryProtocol <NSObject>
+(void)getPullRequestsJavaRepository:(JavaRepositoryModel *)javaRepository success:(void (^) (NSArray<PullRequestModel *> *pullRequests)) _success failure:(void (^) (NSString* error))_failure;
@end
