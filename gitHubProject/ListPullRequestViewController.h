//
//  ListPullRequestViewController.h
//  gitHubProject
//
//  Created by Macbook Pro on 09/10/16.
//  Copyright © 2016 JoaoGabriel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PullRequestModel.h"

@interface ListPullRequestViewController : UIViewController

#pragma mark - properties
@property (nonatomic,strong)NSMutableArray<PullRequestModel *> *listPullRequests;
@property (nonatomic,strong)NSString *repositoryName;
@end
