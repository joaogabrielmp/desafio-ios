//
//  ListPullRequestViewController.m
//  gitHubProject
//
//  Created by Macbook Pro on 09/10/16.
//  Copyright © 2016 JoaoGabriel. All rights reserved.
//

#import "ListPullRequestViewController.h"
#import "ListPullRequestsTableViewDataSource.h"
#import "UIColor+Color.h"
#import "UIFont+AppFont.h"
#import "ListPullRequestsTableViewDataSourceDelegate.h"
@interface ListPullRequestViewController () <ListPullRequestsTableViewDataSourceDelegate>

#pragma mark - ui properties
@property (weak, nonatomic) IBOutlet UILabel *pullRequestsInfoLabel;
@property (weak, nonatomic) IBOutlet UIView *lineView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pullRequestInfoHeightConstraint;

#pragma mark - properties
@property (nonatomic,strong)ListPullRequestsTableViewDataSource *listPullRequestsTableViewDataSource;
@property(nonatomic)int lastContentOffset;

@end

@implementation ListPullRequestViewController

#pragma mark - life cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.title = self.repositoryName;
    self.navigationController.navigationBar.topItem.title = @"";
    [self.tableView.layer setCornerRadius:8.0f];
    self.listPullRequestsTableViewDataSource = [[ListPullRequestsTableViewDataSource alloc] initWithTableView:self.tableView];
    self.listPullRequestsTableViewDataSource.listPullRequest = self.listPullRequests;
    [self.tableView reloadData];
    [self setPullRequestsInfo];
    [self.listPullRequestsTableViewDataSource setDelegate:self];
    [self.pullRequestInfoHeightConstraint setConstant:0.0f];
    [self.view layoutIfNeeded];
    [self.lineView setHidden:YES];
}

-(void)setPullRequestsInfo {
    int openeds = 0;
    int lockeds = 0;
    for (PullRequestModel *pullRequest in self.listPullRequests) {
        if ([pullRequest isOpen]) {
            openeds = openeds + 1;
        }
        if ([pullRequest locked]) {
            lockeds = lockeds + 1;
        }
    }
    
    NSMutableAttributedString *pullRequestInfo = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%i Abertos / ",openeds] attributes:@{NSForegroundColorAttributeName : [UIColor colorWithRGBRed:214 green:131 blue:21 andAlpha:1.0f], NSFontAttributeName : [UIFont getAppSmallBoldItalicFont]}];
    [pullRequestInfo appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%i Fechados",lockeds] attributes:@{NSForegroundColorAttributeName : [UIColor colorWithRGBRed:44 green:44 blue:44 andAlpha:1.0f], NSFontAttributeName : [UIFont getAppSmallBoldItalicFont]}]];
    [self.pullRequestsInfoLabel setAttributedText:[pullRequestInfo copy]];
}


#pragma mark - llist Pull Requests Data Source Delegate

-(void)didSelectJavaRepository:(PullRequestModel *)pullRequest {
   [[UIApplication sharedApplication] openURL:[NSURL URLWithString: pullRequest.url]];
}

-(void)didScroll:(UIScrollView *)scrollView {

    if (scrollView.contentOffset.y < -55) {
        [self.pullRequestInfoHeightConstraint setConstant:41.0f];
        [UIView animateWithDuration:0.4 animations:^{
            [self.lineView setHidden:NO];
            [self.view layoutIfNeeded];
        }];
    }
    if (self.lastContentOffset < scrollView.contentOffset.y && scrollView.contentOffset.y >0)
    {
        [self.pullRequestInfoHeightConstraint setConstant:0.0f];
        [UIView animateWithDuration:0.4 animations:^{
            [self.lineView setHidden:YES];
            [self.view layoutIfNeeded];
        }];

    }

    self.lastContentOffset = scrollView.contentOffset.y;

}

#pragma mark - dealloc
-(void)dealloc {
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
