//
//  ListPullRequestsTableViewDataSource.h
//  gitHubProject
//
//  Created by Macbook Pro on 09/10/16.
//  Copyright © 2016 JoaoGabriel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "PullRequestModel.h"
#import "ListPullRequestsTableViewDataSourceDelegate.h"
@interface ListPullRequestsTableViewDataSource : NSObject<UITableViewDataSource,UITableViewDelegate>

#pragma mark - properties

@property(nonatomic,weak)id<ListPullRequestsTableViewDataSourceDelegate> delegate;
@property(nonatomic,strong)NSMutableArray<PullRequestModel *> *listPullRequest;

#pragma mark - constructors
-(instancetype)initWithTableView:(UITableView *)tableView;


@end
