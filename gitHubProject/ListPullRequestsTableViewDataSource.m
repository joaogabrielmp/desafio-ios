//
//  ListPullRequestsTableViewDataSource.m
//  gitHubProject
//
//  Created by Macbook Pro on 09/10/16.
//  Copyright © 2016 JoaoGabriel. All rights reserved.
//

#import "ListPullRequestsTableViewDataSource.h"
#import "PullRequestCell.h"
@interface ListPullRequestsTableViewDataSource ()
@property(nonatomic,strong)UITableView *tableView;
@end
@implementation ListPullRequestsTableViewDataSource
-(instancetype)initWithTableView:(UITableView *)tableView {
    self = [super init];
    if (self) {
        self.tableView = tableView;
        [self.tableView setDelegate:self];
        [self.tableView setDataSource:self];
    }
    return self;
}

#pragma mark - setters


#pragma mark - table view data source
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.listPullRequest.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 158.0f;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"PullRequestCell";
    PullRequestCell *pullRequestCell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    PullRequestModel *pullRequest = self.listPullRequest[indexPath.row];
    if (pullRequestCell == nil) {
        pullRequestCell = [[PullRequestCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    [pullRequestCell setPullRequest:pullRequest];
    [pullRequestCell setPullRequestInfo];
    return pullRequestCell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
        [self.delegate didSelectJavaRepository:self.listPullRequest[indexPath.row]];
    
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self.delegate didScroll:scrollView];
}
@end
