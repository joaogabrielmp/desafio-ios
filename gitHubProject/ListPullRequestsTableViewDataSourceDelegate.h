//
//  ListPullRequestsTableViewDataSourceDelegate.h
//  gitHubProject
//
//  Created by Macbook Pro on 09/10/16.
//  Copyright © 2016 JoaoGabriel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PullRequestModel.h"
@protocol ListPullRequestsTableViewDataSourceDelegate <NSObject>
-(void)didSelectJavaRepository:(PullRequestModel *)pullRequest;
-(void)didScroll:(UIScrollView *)scrollView;
@end
