//
//  ListRepositoriesJavaRepository.m
//  gitHubProject
//
//  Created by Banco Santander Brasil on 06/10/16.
//  Copyright © 2016 JoaoGabriel. All rights reserved.
//

#import "ListRepositoriesJavaRepository.h"
#import <AFNetworking.h>
#import <Mantle/Mantle.h>
#import "JavaRepositoryModel.h"
@implementation ListRepositoriesJavaRepository
+(void)getJavaRepositoriesInPage:(int)page success:(void (^)(NSArray<JavaRepositoryModel *> *))_success failure:(void (^)(NSString *))_failure{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager GET:[NSString stringWithFormat:@"https://api.github.com/search/repositories?q=language:Java&sort=stars&page=%i",page] parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSArray<JavaRepositoryModel*> *repositories = [MTLJSONAdapter modelsOfClass:[JavaRepositoryModel class] fromJSONArray:responseObject[@"items"] error:nil];
        _success(repositories);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        _failure([error description]);
    }];

}
@end
