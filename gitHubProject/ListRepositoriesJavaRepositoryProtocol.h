//
//  ListRepositoriesJavaRepositoryProtocol.h
//  gitHubProject
//
//  Created by Banco Santander Brasil on 06/10/16.
//  Copyright © 2016 JoaoGabriel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JavaRepositoryModel.h"
@protocol ListRepositoriesJavaRepositoryProtocol <NSObject>
+(void)getJavaRepositoriesInPage:(int)page success:(void (^)(NSArray<JavaRepositoryModel *> *repositories))_success failure:(void (^)(NSString *error))_failure;
@end
