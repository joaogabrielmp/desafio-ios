//
//  ViewController.m
//  gitHubProject
//
//  Created by Banco Santander Brasil on 06/10/16.
//  Copyright © 2016 JoaoGabriel. All rights reserved.
//

#import "ListRepositoriesJavaViewController.h"
#import "ListRepositoriesJavaRepository.h"
#import "ListJavaTableViewDataSource.h"
#import "ListJavaTableViewDataSourceDelegate.h"
#import "ListPullRequestRepository.h"
#import "LoadingViewController.h"
#import "ListPullRequestViewController.h"
#import "LoadingViewControllerDelegate.h"
@interface ListRepositoriesJavaViewController ()<ListJavaTableViewDataSourceDelegate,LoadingViewControllerDelegate>
#pragma mark - ui properties
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *activityIndicatorHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *activityIndicatorBottomConstraint;

#pragma mark - properties
@property (nonatomic)int page;
@property (nonatomic,strong)ListJavaTableViewDataSource *listJavaTableViewDataSource;
@property (nonatomic,strong)ListPullRequestViewController *listPullRequestViewController;
@property (nonatomic,strong)NSMutableArray<JavaRepositoryModel *> *listJavaRepository;
@property(nonatomic)BOOL isLoading;
@end

@implementation ListRepositoriesJavaViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.page = 1;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.title = @"Lista de Repositórios";
    self.listJavaTableViewDataSource = [[ListJavaTableViewDataSource alloc] initWithTableView:self.tableView];
    [self.tableView.layer setCornerRadius:8.0f];
    self.listJavaRepository = [[NSMutableArray alloc]init];
    [self.listJavaTableViewDataSource setDelegate:self];
    [self callListJavaRepositories];
    [self setRefreshNavBar];
    [self hideTableView];
}

#pragma mark - refresh
-(void)setRefreshNavBar {

    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(refreshRepositories)];
}

#pragma mark - call services
-(void)refreshRepositories {
    self.listJavaRepository = [[NSMutableArray alloc]init];
    self.page = 1;
    [self.tableView setScrollsToTop:YES];
    [self hideTableView];
    [self callListJavaRepositories];
}

-(void)callListJavaRepositories {
    if (!self.isLoading) {
        [self setIsLoading:YES];
        [ListRepositoriesJavaRepository getJavaRepositoriesInPage:self.page success:^(NSArray<JavaRepositoryModel *> *repositories) {
            [self showTableView];
            [self hideLoading];
            [self setIsLoading:NO];
            self.page = self.page + 1;
            [self.listJavaRepository addObjectsFromArray:repositories];
            self.listJavaTableViewDataSource.listJavaRepository = self.listJavaRepository;
            [self.tableView reloadData];

        } failure:^(NSString *error) {
            [self hideLoading];
            [self setIsLoading:NO];
            [[[UIAlertView alloc] initWithTitle:@"Atenção" message:@"Ocorreu algum problema no serviço" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
            
        }];
    }
}

#pragma mark - layout
-(void)hideTableView {
    [self.tableView setHidden:YES];
    [self.activityIndicatorHeightConstraint setConstant:20.0f];
    [self.activityIndicator startAnimating];
    [self.activityIndicator setHidden:NO];

    [self.activityIndicatorBottomConstraint setConstant:self.view.frame.size.height/2];
    [UIView animateWithDuration:0.8 delay:0.0 usingSpringWithDamping:10.0f initialSpringVelocity:20.f options:UIViewAnimationOptionCurveEaseInOut animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        
    }];
    
}

-(void)showTableView {
    [self.tableView setHidden:NO];
    [self.activityIndicator stopAnimating];
    [self.activityIndicator setHidden:YES];

}

-(void)showLoading {
    [self.activityIndicatorHeightConstraint setConstant:20.0f];
    [self.activityIndicatorBottomConstraint setConstant:8.0f];
    [self.activityIndicator startAnimating];
    [UIView animateWithDuration:0.8 delay:0.0 usingSpringWithDamping:10.0f initialSpringVelocity:20.f options:UIViewAnimationOptionCurveEaseInOut animations:^{
         [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        
    }];
   
}

-(void)hideLoading {
    [self.activityIndicatorHeightConstraint setConstant:0.0f];
    [self.activityIndicatorBottomConstraint setConstant:0.0f];
    [UIView animateWithDuration:0.8 delay:0.0 usingSpringWithDamping:10.0f initialSpringVelocity:20.0f options:UIViewAnimationOptionCurveEaseInOut animations:^{
        [self.view layoutIfNeeded];
        [self.activityIndicator stopAnimating];
    } completion:^(BOOL finished) {
        
    }];
  

}
#pragma mark - list repository datasource delegate
-(void)didSelectJavaRepository:(JavaRepositoryModel *)javaRepository {
    LoadingViewController *loading = [[LoadingViewController alloc]init];
    [loading setDelegate:self];
    loading.animationType = PopoverViewControllerAnimationTypeCoverVertical;
    loading.shadow = YES;
    loading.shouldDismissOnBackgroundTap = NO;
    loading.cornerRadius = 12.0f;
    [loading.infoLabel setText:@"Carregando.."];
    [loading showLoadingInViewController:self];
   
    [ListPullRequestRepository getPullRequestsJavaRepository:javaRepository success:^(NSArray<PullRequestModel *> *pullRequests) {
        [loading hideLoading];
        
        self.listPullRequestViewController = (ListPullRequestViewController*)[[UIStoryboard storyboardWithName:@"Main" bundle:nil]
                                                                                        instantiateViewControllerWithIdentifier: @"listPullRequestViewController"];
        self.listPullRequestViewController.listPullRequests = [pullRequests mutableCopy];
        [self.listPullRequestViewController setRepositoryName:javaRepository.name];

    } failure:^(NSString *error) {

        [loading hideLoading];
          [[[UIAlertView alloc] initWithTitle:@"Atenção" message:@"Ocorreu algum problema no serviço" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
    }];
}

-(void)didScroll:(UIScrollView *)scrollView {
    CGPoint offset = scrollView.contentOffset;
    CGRect bounds = scrollView.bounds;
    CGSize size = scrollView.contentSize;
    UIEdgeInsets inset = scrollView.contentInset;
    float scrollViewPositionY = offset.y + bounds.size.height - inset.bottom;
    float scrollViewHeight = size.height;
   
    
    float reload_distance = -50;
    if(scrollViewPositionY > scrollViewHeight + reload_distance) {
        if (!self.isLoading) {
            [self showLoading];
            [self callListJavaRepositories];
        }

    }
}

#pragma mark - loading delegate
-(void)didFinishDisappear {
    [self.navigationController pushViewController:self.listPullRequestViewController animated:YES]; 
}

#pragma mark - dealloc
-(void)dealloc {
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
