//
//  LoadingViewController.h
//  gitHubProject
//
//  Created by Macbook Pro on 09/10/16.
//  Copyright © 2016 JoaoGabriel. All rights reserved.
//

#import "PopoverViewController.h"
#import "LoadingViewControllerDelegate.h"
@interface LoadingViewController : PopoverViewController

#pragma mark - properties
@property (nonatomic,weak)id<LoadingViewControllerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

-(void)hideLoading;
-(void)showLoadingInViewController:(UIViewController *)viewController;
@end
