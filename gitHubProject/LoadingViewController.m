//
//  LoadingViewController.m
//  gitHubProject
//
//  Created by Macbook Pro on 09/10/16.
//  Copyright © 2016 JoaoGabriel. All rights reserved.
//

#import "LoadingViewController.h"

@interface LoadingViewController ()

@end

@implementation LoadingViewController

#pragma mark - constructors
-(instancetype)init{
    if (self = [super initWithSize:CGSizeMake([UIScreen mainScreen].bounds.size.width * .6, 100)]) {
           }
    return self;
}

#pragma mark - life cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    [self.activityIndicator startAnimating];
}


#pragma mark - methods
-(void)hideLoading {
    [self dismissViewControllerAnimated:YES completion:^{
        [self.delegate didFinishDisappear];
    }];
}

-(void)showLoadingInViewController:(UIViewController *)viewController {
    [viewController presentViewController:self animated:YES completion:nil];
}

#pragma mark - dealloc
-(void)dealloc {
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
