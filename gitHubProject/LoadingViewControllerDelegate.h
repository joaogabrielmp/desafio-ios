//
//  LoadingViewControllerDelegate.h
//  gitHubProject
//
//  Created by Banco Santander Brasil on 10/10/16.
//  Copyright © 2016 JoaoGabriel. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol LoadingViewControllerDelegate <NSObject>
-(void)didFinishDisappear;
@end
