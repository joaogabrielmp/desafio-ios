//
//  PullRequestCell.h
//  gitHubProject
//
//  Created by Banco Santander Brasil on 06/10/16.
//  Copyright © 2016 JoaoGabriel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PullRequestModel.h"
@interface PullRequestCell : UITableViewCell
#pragma mark - properties
@property(nonatomic,strong)PullRequestModel *pullRequest;

#pragma mark - methods
-(void)setPullRequestInfo;
@end
