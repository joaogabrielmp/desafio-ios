//
//  PullRequestCell.m
//  gitHubProject
//
//  Created by Banco Santander Brasil on 06/10/16.
//  Copyright © 2016 JoaoGabriel. All rights reserved.
//

#import "PullRequestCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "UIFont+AppFont.h"
@interface PullRequestCell ()
@property (weak, nonatomic) IBOutlet UILabel *titlePullRequestLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionPullRequestLabel;
@property (weak, nonatomic) IBOutlet UIImageView *userImageView;
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet UILabel *dataLabel;
@end
@implementation PullRequestCell

#pragma mark - constructors
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    self = [[NSBundle mainBundle]loadNibNamed:NSStringFromClass([self class]) owner:self options:nil].firstObject;
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setLayout];
        
    }
    return self;
}

#pragma mark - initializers
-(void)setPullRequestInfo {
    [self.titlePullRequestLabel setText:self.pullRequest.title];
    if (self.pullRequest.bodyPR != [NSNull null] || !self.pullRequest.bodyPR) {
        [self.descriptionPullRequestLabel setText:self.pullRequest.bodyPR];
    } else {
       [self.descriptionPullRequestLabel setText:@"Sem Descrição"];
    }
    [self.usernameLabel setText:self.pullRequest.user.name];
    
    
    [self.dataLabel setText:[self.pullRequest formattedDatePR]];
    if (self.pullRequest.user.userPhoto != [NSNull null] || !self.pullRequest.user.userPhoto) {
        [self.userImageView sd_setImageWithURL:[NSURL URLWithString:self.pullRequest.user.userPhoto]
                              placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    } else {
        [self.userImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://placehold.it/%.f?text=Nenhuma+Imagem+Encontrada",self.imageView.frame.size.width]]
                              placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    }
}

#pragma mark - layout
-(void)setLayout {
    [self.titlePullRequestLabel setFont:[UIFont getAppSmallBoldItalicFont]];
    [self.descriptionPullRequestLabel setFont:[UIFont getAppSmallItalicFont]];
    [self.usernameLabel setFont:[UIFont getAppSmallBoldItalicFont]];
    [self.dataLabel setFont:[UIFont getAppSmallItalicFont]];
   
}
@end
