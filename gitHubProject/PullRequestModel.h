//
//  PullRequestModel.h
//  gitHubProject
//
//  Created by Macbook Pro on 09/10/16.
//  Copyright © 2016 JoaoGabriel. All rights reserved.
//

#import <Mantle/Mantle.h>
#import "PullRequestUser.h"
#import "Base.h"
@interface PullRequestModel : MTLModel<MTLJSONSerializing>
@property (nonatomic,strong) PullRequestUser *user;
@property (nonatomic,strong) Base *base;
@property (nonatomic,strong) NSString *title;
@property (nonatomic,strong) NSString *datePR;
@property (nonatomic,strong) NSString *bodyPR;
@property (nonatomic,strong) NSString *state;
@property (nonatomic,strong) NSString *url;
@property (nonatomic) BOOL locked;

-(BOOL)isOpen;
-(NSString *)formattedDatePR;
@end
