//
//  PullRequestModel.m
//  gitHubProject
//
//  Created by Macbook Pro on 09/10/16.
//  Copyright © 2016 JoaoGabriel. All rights reserved.
//

#import "PullRequestModel.h"

@implementation PullRequestModel
+(NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{@"title":@"title",
             @"base":@"base",
             @"user":@"user",
             @"bodyPR":@"body",
             @"datePR":@"created_at",
             @"state":@"state",
             @"locked":@"locked",
             @"url":@"html_url"};
}

-(BOOL)isOpen {
    return [self.state isEqualToString:@"open"];
}

-(NSString *)formattedDatePR {

    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
    NSDate *date = [dateFormat dateFromString:self.datePR];
    [dateFormat setDateFormat:@"dd/MM/yyyy HH:mm:ss"];
    return [dateFormat stringFromDate:date];
}

+(NSValueTransformer *)userJSONTransformer
{
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:[PullRequestUser class]];
}

+ (NSValueTransformer *)baseJSONTransformer
{
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:[Base class]];
}
@end
