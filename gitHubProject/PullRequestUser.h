//
//  PullRequestUser.h
//  gitHubProject
//
//  Created by Macbook Pro on 09/10/16.
//  Copyright © 2016 JoaoGabriel. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface PullRequestUser : MTLModel<MTLJSONSerializing>
@property(nonatomic,strong)NSString *userPhoto;
@property(nonatomic,strong)NSString *name;

@end
