//
//  PullRequestUser.m
//  gitHubProject
//
//  Created by Macbook Pro on 09/10/16.
//  Copyright © 2016 JoaoGabriel. All rights reserved.
//

#import "PullRequestUser.h"

@implementation PullRequestUser
+(NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"userPhoto":@"avatar_url",
             @"name":@"login"
             };
}


@end
