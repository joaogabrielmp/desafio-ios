//
//  RepositoryCell.h
//  gitHubProject
//
//  Created by Banco Santander Brasil on 06/10/16.
//  Copyright © 2016 JoaoGabriel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JavaRepositoryModel.h"
@interface RepositoryCell : UITableViewCell
#pragma mark - properties
@property(nonatomic,strong)JavaRepositoryModel *javaRepository;

#pragma mark - methods
-(void)setRepositoryInfo;
@end
