//
//  RepositoryCell.m
//  gitHubProject
//
//  Created by Banco Santander Brasil on 06/10/16.
//  Copyright © 2016 JoaoGabriel. All rights reserved.
//

#import "RepositoryCell.h"
#import "UIFont+AppFont.h"
#import "UIColor+Color.h"
#import <SDWebImage/UIImageView+WebCache.h>
@interface RepositoryCell ()

#pragma mark - ui properties
@property (weak, nonatomic) IBOutlet UILabel *nameRepositoryLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionRepositoryLabel;
@property (weak, nonatomic) IBOutlet UILabel *forkLabel;
@property (weak, nonatomic) IBOutlet UILabel *starLabel;
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *forkImageView;
@property (weak, nonatomic) IBOutlet UIImageView *startImageView;
@property (weak, nonatomic) IBOutlet UIImageView *userImageView;


@end
@implementation RepositoryCell
#pragma mark - constructors
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    self = [[NSBundle mainBundle]loadNibNamed:NSStringFromClass([self class]) owner:self options:nil].firstObject;
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setLayout];
    }
    return self;
}

#pragma mark - initializers
-(void)setRepositoryInfo {
    [self.nameRepositoryLabel setText:self.javaRepository.name];
    [self.descriptionRepositoryLabel setText:self.javaRepository.repositoryDescription];
    [self.forkLabel setText:[NSString stringWithFormat:@"%.f",self.javaRepository.forks]];
    [self.starLabel setText:[NSString stringWithFormat:@"%.f",self.javaRepository.stars]];
    [self.usernameLabel setText:self.javaRepository.user.name];
    if (self.javaRepository.user.userPhoto != [NSNull null] || !self.javaRepository.user.userPhoto) {
        [self.userImageView sd_setImageWithURL:[NSURL URLWithString:self.javaRepository.user.userPhoto]
                          placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    } else {
        [self.userImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://placehold.it/%.f?text=Nenhuma+Imagem+Encontrada",self.imageView.frame.size.width]]
                          placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    }
}

#pragma mark - layout
-(void)setLayout {
    [self.nameRepositoryLabel setFont:[UIFont getAppBoldItalicFont]];
    [self.descriptionRepositoryLabel setFont:[UIFont getAppItalicFont]];
    [self.forkLabel setFont:[UIFont getAppBoldItalicFont]];
    [self.starLabel setFont:[UIFont getAppBoldItalicFont]];
    [self.usernameLabel setFont:[UIFont getAppSmallItalicFont]];
    
}

@end
