//
//  UIColor+Color.h
//  Rankim
//
//  Created by Rafael Gonzalves on 3/30/16.
//  Copyright © 2016 Macbook Pro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Color)
+(UIColor *)colorWithRGBRed:(float)red green:(float)green blue:(float)blue andAlpha:(float)alpha;

+(UIColor *)AGT_primaryColor;
+(UIColor *)AGT_primaryColorDark;
+(UIColor *)AGT_primaryColorLight;
@end
