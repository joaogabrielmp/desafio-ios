//
//  UIColor+Color.m
//  Rankim
//
//  Created by Rafael Gonzalves on 3/30/16.
//  Copyright © 2016 Macbook Pro. All rights reserved.
//

#import "UIColor+Color.h"

@implementation UIColor (Color)
+(UIColor *)colorWithRGBRed:(float)red green:(float)green blue:(float)blue andAlpha:(float)alpha {
    return [UIColor colorWithRed:red/255.0f green:green/255.0f blue:blue/255.0f alpha:alpha];
}

+(UIColor *)AGT_primaryColor {
    return  [UIColor colorWithRGBRed:41 green:101 blue:158 andAlpha:1.000];
}
+(UIColor *)AGT_primaryColorDark {
    return [UIColor colorWithRGBRed:25 green:85 blue:152 andAlpha:1.000];
}
+(UIColor *)AGT_primaryColorLight {
    return [UIColor colorWithRGBRed:57 green:117 blue:174 andAlpha:0.600];
}


@end
