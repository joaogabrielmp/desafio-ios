//
//  UIFont+AppFont.h
//  Agenda
//
//  Created by Banco Santander Brasil on 15/08/16.
//  Copyright © 2016 Macbook Pro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont (AppFont)

+(UIFont *)getAppFont;
+(UIFont *)getAppSmallFont;
+(UIFont *)getAppBoldFont;
+(UIFont *)getAppSmallBoldFont;
+(UIFont *)getAppBoldItalicFont;
+(UIFont *)getAppSmallBoldItalicFont;
+(UIFont *)getAppBigBoldItalicFont;
+(UIFont *)getAppItalicFont;
+(UIFont *)getAppSmallItalicFont;

@end
