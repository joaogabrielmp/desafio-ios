//
//  UIFont+AppFont.m
//  Agenda
//
//  Created by Banco Santander Brasil on 15/08/16.
//  Copyright © 2016 Macbook Pro. All rights reserved.
//

#import "UIFont+AppFont.h"

@implementation UIFont (AppFont)
+(NSString *)getAppFontName {
    return @"ArialMT";
}
+(NSString *)getAppBoldItalicFontName {
    return @"Arial-BoldItalicMT";
}
+(NSString *)getAppBoldFontName {
    return @"Arial-BoldMT";
}
+(NSString *)getAppItalicFontName {
    return @"Arial-ItalicMT";
}

+(UIFont *)getAppFont {
    return [UIFont fontWithName:[self getAppFontName] size:15.0f];
}

+(UIFont *)getAppSmallFont {
    return [UIFont fontWithName:[self getAppFontName] size:13.0f];
}

+(UIFont *)getAppBoldFont {
    return [UIFont fontWithName:[self getAppBoldFontName] size:15.0f];
}
+(UIFont *)getAppSmallBoldFont {
    return [UIFont fontWithName:[self getAppBoldFontName] size:13.0f];
}

+(UIFont *)getAppBoldItalicFont {
    return [UIFont fontWithName:[self getAppBoldItalicFontName] size:15.0f];
}

+(UIFont *)getAppSmallBoldItalicFont {
    return [UIFont fontWithName:[self getAppBoldItalicFontName] size:12.0f];
}

+(UIFont *)getAppBigBoldItalicFont {
    return [UIFont fontWithName:[self getAppBoldItalicFontName] size:17.0f];
}

+(UIFont *)getAppItalicFont {
    return [UIFont fontWithName:[self getAppItalicFontName] size:15.0f];
}
+(UIFont *)getAppSmallItalicFont {
    return [UIFont fontWithName:[self getAppItalicFontName] size:12.0f];
}
@end
