//
//  User.h
//  gitHubProject
//
//  Created by Banco Santander Brasil on 07/10/16.
//  Copyright © 2016 JoaoGabriel. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface User : MTLModel<MTLJSONSerializing>
@property(nonatomic,strong)NSString *userPhoto;
@property(nonatomic,strong)NSString *name;
@end
