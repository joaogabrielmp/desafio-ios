//
//  User.m
//  gitHubProject
//
//  Created by Banco Santander Brasil on 07/10/16.
//  Copyright © 2016 JoaoGabriel. All rights reserved.
//

#import "User.h"

@implementation User
+(NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"userPhoto":@"avatar_url",
             @"name":@"login"
             };
}
@end
