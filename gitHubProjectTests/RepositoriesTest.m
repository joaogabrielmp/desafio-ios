//
//  ListJavaRepositoriesTest.m
//  gitHubProject
//
//  Created by Banco Santander Brasil on 10/10/16.
//  Copyright © 2016 JoaoGabriel. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "JavaRepositoryModel.h"
#import "PullRequestModel.h"
#import "User.h"
#import "ListRepositoriesJavaRepository.h"
#import "ListPullRequestRepository.h"
@interface RepositoriesTest : XCTestCase
@property(nonatomic,strong)NSMutableArray<JavaRepositoryModel *> *listJavaRepository;
@property(nonatomic,strong)NSMutableArray<PullRequestModel *> *listPullRequest;
@end

@implementation RepositoriesTest

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testListRepositoriesService {
    int page = 1 ;
    [ListRepositoriesJavaRepository getJavaRepositoriesInPage:page success:^(NSArray<JavaRepositoryModel *> *repositories) {
        self.listJavaRepository = [repositories mutableCopy];
        XCTAssertNotNil(self.listJavaRepository,@"Possui lista de java repository");
        XCTAssertTrue(self.listJavaRepository.count > 0,@"Quantidade de java repository maior que 0");
        for (JavaRepositoryModel *javaRepository in self.listJavaRepository) {
            XCTAssertNotNil(javaRepository,@"Possui um java repository");
            [self javaRepository:javaRepository];
            [self testListPullRequestService:javaRepository];
        }
    } failure:^(NSString *error) {
        XCTAssertNotNil(error,@"Possui mensagem de erro");
    }];
}
-(void)javaRepository:(JavaRepositoryModel *)javaRepository {
    XCTAssertNotNil(javaRepository,@"Possui um java repository");
    XCTAssertNotNil(javaRepository.name,@"Possui nome do java repository");
    XCTAssertNotNil(javaRepository.repositoryDescription,@"Possui descricao do javaRepository");
    XCTAssertNotNil(javaRepository.user,@"Possui user");
    XCTAssertNotNil(javaRepository.user.name,@"Possui user name");
    XCTAssertNotNil(javaRepository.user.userPhoto,@"Possui foto do usuario");
    XCTAssertTrue(javaRepository.stars,@"Possui stars");
    XCTAssertTrue(javaRepository.stars> 0,@"Quantidade de stars maior que 0");
    XCTAssertTrue(javaRepository.forks,@"Possui forks");
    XCTAssertTrue(javaRepository.forks > 0,@"Quantidade de forks maior que 0");
}

- (void)testListPullRequestService:(JavaRepositoryModel *)javaRepository {
    XCTAssertNotNil(javaRepository,@"Possui java repository");
    if (!javaRepository) {
        [javaRepository setStars:38686];
        [javaRepository setForks:8642];
        [javaRepository setRepositoryDescription:@"Descricao"];
        User *user = [[User alloc]init];
        [user setName:@"facebook"];
        [user setUserPhoto:@"https://avatars.githubusercontent.com/u/69631?v=3"];
        [javaRepository setUser:user];
        [javaRepository setName:@"react-native"];
    }
    [ListPullRequestRepository getPullRequestsJavaRepository:javaRepository success:^(NSArray<PullRequestModel *> *pullRequests) {
        self.listPullRequest = [pullRequests mutableCopy];
        XCTAssertNotNil(self.listPullRequest,@"Possui pull requests");
        XCTAssertTrue(self.listPullRequest.count > 0,@"Quantidade de pull requests maior que 0");

        for (PullRequestModel *pullRequest in self.listPullRequest) {
            XCTAssertNotNil(pullRequest,@"Possui pull request");
            [self pullRequest:pullRequest];
        }
    } failure:^(NSString *error) {
        XCTAssertNotNil(error,@"Possui mensagem de erro");
    }];
}

-(void)pullRequest:(PullRequestModel *)pullRequest {
    XCTAssertNotNil(pullRequest.user,@"Possui pull request usuario");
    XCTAssertNotNil(pullRequest.user.name,@"Possui pull request nome usuario");
    XCTAssertNotNil(pullRequest.user.userPhoto,@"Possui pull request foto de usuario");
    XCTAssertNotNil(pullRequest.base,@"Possui pull request Base");
    XCTAssertNotNil(pullRequest.base.repository,@"Possui pull request repository");
    XCTAssertNotNil(pullRequest.base.repository.nameRepository,@"Possui pull request repository name");
    XCTAssertNotNil(pullRequest.title,@"Possui pull request titulo");
    XCTAssertNotNil(pullRequest.datePR,@"Possui pull request data");
    XCTAssertNotNil(pullRequest.bodyPR,@"Possui pull request body");
    XCTAssertNotNil(pullRequest.state,@"Possui pull request estado(aberto,fechado)");
    XCTAssertNotNil(pullRequest.url,@"Possui pull request url");
    XCTAssertTrue(pullRequest.locked,@"Possui pull request fechado");
    XCTAssertTrue([pullRequest isOpen],@"Possui pull request aberto");
}



@end
